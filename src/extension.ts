// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

import { ExtractMethod } from './actions/ExtractMethod';
import { RenameClassProperty } from './actions/RenameClassProperty';
import { RenameLocalVariable } from './actions/RenameLocalVariable';

/**
 * @param {vscode.ExtensionContext} context
 */
export function activate(context: vscode.ExtensionContext) {
	console.log('Congratulations, your extension "PHP Refactoring" is now active!');

	let extractMethodCommand = vscode.commands.registerCommand('extension.extractMethod', () => {
		let extractMethod = new ExtractMethod();
		extractMethod.refactor(context);
	});

	context.subscriptions.push(extractMethodCommand);

	let renameLocalVariableCommand = vscode.commands.registerCommand('extension.renameLocalVariable', () => {
		let renameLocalVariable = new RenameLocalVariable();
		renameLocalVariable.refactor(context);
	});

	context.subscriptions.push(renameLocalVariableCommand);

	let renameClassPropertyCommand = vscode.commands.registerCommand('extension.renameClassProperty', () => {
		let renameClassProperty = new RenameClassProperty();
		renameClassProperty.refactor(context);
	});

	context.subscriptions.push(renameClassPropertyCommand);
}

export function deactivate() {}
