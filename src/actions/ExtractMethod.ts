import * as vscode from 'vscode';

import { Editor } from '../commons/Editor';
import { FileSystem } from '../commons/FileSystem';
import { SystemShell } from '../commons/SystemShell';

export class ExtractMethod {

    private editor: Editor;
    private fileSystem: FileSystem;
    private systemShell: SystemShell;

    constructor() {
        this.editor = new Editor();
        this.fileSystem = new FileSystem();
        this.systemShell = new SystemShell();
    }

    public refactor(context: vscode.ExtensionContext): void {
        let thisClass = this;
        vscode.window.showInputBox({ prompt: "Enter the name of the new method." }).then(function (methodName: any) {
            if (methodName !== null && methodName.length > 0) {
                thisClass.extractMethod(methodName, context);
            }
            else {
                vscode.window.showErrorMessage("The method name has to be longer than 0 characters.");
            }
        });
    }

    private extractMethod(methodName: string, context: vscode.ExtensionContext): void {
        if (vscode.window.activeTextEditor !== null && this.editor.isLineRangeSelected()) {       
            let startLine = this.editor.getStartLine();
            let endLine = this.editor.getEndLine();
            let fileLocation = this.fileSystem.getFileLocation();
            if (fileLocation !== null) {
                let executionCommand = "extract-method " + fileLocation + " " + startLine + "-" + endLine + " " + methodName;
                let completeText = this.editor.getCompleteText();
                this.systemShell.executeCommand(executionCommand, context, completeText);
            }
            else {
                vscode.window.showErrorMessage("You cannot do a refactoring on an unsaved file. Please save the file to the disk first.");
            }
        }
        else {
            vscode.window.showErrorMessage("There is nothing selected to refactor.");
        }
    }
}
