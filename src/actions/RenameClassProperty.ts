import * as vscode from 'vscode';

import { Editor } from '../commons/Editor';
import { FileSystem } from '../commons/FileSystem';
import { SystemShell } from '../commons/SystemShell';

export class RenameClassProperty {

    private editor: Editor;
    private fileSystem: FileSystem;
    private systemShell: SystemShell;

    constructor() {
        this.editor = new Editor();
        this.fileSystem = new FileSystem();
        this.systemShell = new SystemShell();
    }

    public refactor(context: vscode.ExtensionContext): void {
        let thisClass = this;
        vscode.window.showInputBox({ prompt: "Enter the new name of the class property." }).then(function (classProperty: any) {
            if (classProperty !== null && classProperty!.length > 0) {
                thisClass.renameClassProperty(classProperty, context);
            }
            else {
                vscode.window.showErrorMessage("The new class property name has to be longer than 0 characters.");
            }
        });
    }

    private renameClassProperty(classProperty: string, context: vscode.ExtensionContext): void {
        if (vscode.window.activeTextEditor !== null && this.editor.isLineRangeSelected()) {       
            let line = this.editor.getStartLine();
            let oldClassProperty = this.editor.getSelectedText();
            let fileLocation = this.fileSystem.getFileLocation();
            if (fileLocation !== null) {
                let executionCommand = "rename-property " + fileLocation + " " + line + " " + oldClassProperty + " " + classProperty;
                let completeText = this.editor.getCompleteText();
                this.systemShell.executeCommand(executionCommand, context, completeText);
            }
            else {
                vscode.window.showErrorMessage("You cannot do a refactoring on an unsaved file. Please save the file to the disk first.");
            }
        }
        else {
            vscode.window.showErrorMessage("There is nothing selected to refactor.");
        }
    }
}
