import * as vscode from 'vscode';

import { Editor } from '../commons/Editor';
import { FileSystem } from '../commons/FileSystem';
import { SystemShell } from '../commons/SystemShell';

export class RenameLocalVariable {

    private editor: Editor;
    private fileSystem: FileSystem;
    private systemShell: SystemShell;

    constructor() {
        this.editor = new Editor();
        this.fileSystem = new FileSystem();
        this.systemShell = new SystemShell();
    }

    public refactor(context: vscode.ExtensionContext): void {
        let thisClass = this;
        vscode.window.showInputBox({ prompt: "Enter the new name of the variable." }).then(function (variableName: any) {
            if (variableName !== null && variableName!.length > 0) {
                thisClass.renameLocalVariable(variableName, context);
            }
            else {
                vscode.window.showErrorMessage("The new variable name has to be longer than 0 characters.");
            }
        });
    }

    private renameLocalVariable(variableName: string, context: vscode.ExtensionContext): void {
        if (vscode.window.activeTextEditor !== null && this.editor.isLineRangeSelected()) {       
            let line = this.editor.getStartLine();
            let oldVariableName = this.editor.getSelectedText();
            let fileLocation = this.fileSystem.getFileLocation();
            if (fileLocation !== null) {
                let executionCommand = "rename-local-variable " + fileLocation + " " + line + " " + oldVariableName + " " + variableName;
                let completeText = this.editor.getCompleteText();
                this.systemShell.executeCommand(executionCommand, context, completeText);
            }
            else {
                vscode.window.showErrorMessage("You cannot do a refactoring on an unsaved file. Please save the file to the disk first.");
            }
        }
        else {
            vscode.window.showErrorMessage("There is nothing selected to refactor.");
        }
    }
}
