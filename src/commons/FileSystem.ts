import * as vscode from 'vscode';

export class FileSystem {
    public getFileLocation(): string {
        let fileLocation = vscode.window.activeTextEditor?.document.uri.fsPath;
        if (fileLocation!.startsWith("Untitled")) {
            return null as any;
        }
        return fileLocation as any;
    }

    public getPHPExecutable(): string {

        const conf = vscode.workspace.getConfiguration('php');

        let executablePath: string =
        conf.get('executablePath') ||
        conf.get('validate.executablePath') ||
        (process.platform === 'win32' ? 'php.exe' : 'php');

        if (executablePath === null || executablePath === "") {
            vscode.window.showErrorMessage("Cannot find the PHP executable path. Please set it as environment variable or in the Visual Studio Code settings.");
        }

        return executablePath;
    }

    public getLibraryLocation(context: vscode.ExtensionContext): string {
        return context.asAbsolutePath("refactor.phar");
    }
}