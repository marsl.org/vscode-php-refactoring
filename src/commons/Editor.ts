import * as vscode from 'vscode';

export class Editor {
    public getCompleteText(): string {
        let completeText = vscode.window.activeTextEditor!.document.getText();
        return completeText;
    }

    public getStartLine(): number {
        let startLine = vscode.window.activeTextEditor!.selection.start.line + 1;
        return startLine;
    }

    public getEndLine(): number {
        let endLine = vscode.window.activeTextEditor!.selection.end.line + 1;
        return endLine;
    }

    public getSelectedText(): string {
        let selection = vscode.window.activeTextEditor?.selection;
        return vscode.window.activeTextEditor!.document.getText(selection);
    }

    public isLineRangeSelected(): boolean {
        let startLine = this.getStartLine();
        let endLine = this.getEndLine();
        let result = false;
        if (startLine !== endLine) {
            result = true;
        }
        else {
            let selection = vscode.window.activeTextEditor?.selection;
            let text = vscode.window.activeTextEditor?.document.getText(selection);
            result = text!.length > 0;
        }
        return result;
    }
}
