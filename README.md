# VSCode PHP Refactoring README

This is the README for the VSCode PHP Refactoring extension.

The plugin is based on the [PHP Refactoring Browser by QafooLabs](https://qafoolabs.github.io/php-refactoring-browser/) with its [latest version by ShahinSorkh](https://github.com/ShahinSorkh/refactor.phar).

## Features

### Extract Method

You can use the Extract Method refactoring in this way: Select the code lines you want to extract to a method. Then use the command 'PHP: Extract Method'. Enter the new method name.
The new method will be created outside the scope of your current method and will be called at your current location.

### Rename Local Variable

You can use Rename Local Variable refactoring in this way: Select the variable with or without the dollar sign you want to rename. Then use the command 'PHP: Rename Local Variable'.
Enter the new variable name with or without the dollar sign. Every occurence of the variable in the specific block will be renamed.

### Rename Class Property

You can use Rename Class Property refactoring in ths way: Select the class property with or without the dollar sign you want to rename. Then use the command 'PHP: Rename Class Property'.
Enter the new class property name with or without the dollar sign. Every occurence of the class property in the specific class will be renamed.
WARNING: Right now only private class properties can be properly renamed. If the class property is public and used in other files and classes this refactoring will not work.

## Requirements

Please note. PHP need to be installed on your machine and it should be set in the environment path or in the Visual Studio Code settings.

## Release Notes

### 1.3.0

Added support for PHP 7 and PHP 8.

### 1.2.0

Added support for Rename Class Property refactoring.

### 1.1.0

Added support for Rename Local Variable refactoring.

### 1.0.0

Initial release of the VSCode PHP Refactoring extension with support for Extract Method refactoring.

## Support me

If you like this extension for VS Code and want to invite me for a virtual beer or slice of cake, you can make a donation:

[![Paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif)](https://paypal.me/mlinke86)
