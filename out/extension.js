"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = void 0;
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require("vscode");
const ExtractMethod_1 = require("./actions/ExtractMethod");
const RenameClassProperty_1 = require("./actions/RenameClassProperty");
const RenameLocalVariable_1 = require("./actions/RenameLocalVariable");
/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {
    console.log('Congratulations, your extension "PHP Refactoring" is now active!');
    let extractMethodCommand = vscode.commands.registerCommand('extension.extractMethod', () => {
        let extractMethod = new ExtractMethod_1.ExtractMethod();
        extractMethod.refactor(context);
    });
    context.subscriptions.push(extractMethodCommand);
    let renameLocalVariableCommand = vscode.commands.registerCommand('extension.renameLocalVariable', () => {
        let renameLocalVariable = new RenameLocalVariable_1.RenameLocalVariable();
        renameLocalVariable.refactor(context);
    });
    context.subscriptions.push(renameLocalVariableCommand);
    let renameClassPropertyCommand = vscode.commands.registerCommand('extension.renameClassProperty', () => {
        let renameClassProperty = new RenameClassProperty_1.RenameClassProperty();
        renameClassProperty.refactor(context);
    });
    context.subscriptions.push(renameClassPropertyCommand);
}
exports.activate = activate;
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map