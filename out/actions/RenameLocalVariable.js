"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RenameLocalVariable = void 0;
const vscode = require("vscode");
const Editor_1 = require("../commons/Editor");
const FileSystem_1 = require("../commons/FileSystem");
const SystemShell_1 = require("../commons/SystemShell");
class RenameLocalVariable {
    constructor() {
        this.editor = new Editor_1.Editor();
        this.fileSystem = new FileSystem_1.FileSystem();
        this.systemShell = new SystemShell_1.SystemShell();
    }
    refactor(context) {
        let thisClass = this;
        vscode.window.showInputBox({ prompt: "Enter the new name of the variable." }).then(function (variableName) {
            if (variableName !== null && variableName.length > 0) {
                thisClass.renameLocalVariable(variableName, context);
            }
            else {
                vscode.window.showErrorMessage("The new variable name has to be longer than 0 characters.");
            }
        });
    }
    renameLocalVariable(variableName, context) {
        if (vscode.window.activeTextEditor !== null && this.editor.isLineRangeSelected()) {
            let line = this.editor.getStartLine();
            let oldVariableName = this.editor.getSelectedText();
            let fileLocation = this.fileSystem.getFileLocation();
            if (fileLocation !== null) {
                let executionCommand = "rename-local-variable " + fileLocation + " " + line + " " + oldVariableName + " " + variableName;
                let completeText = this.editor.getCompleteText();
                this.systemShell.executeCommand(executionCommand, context, completeText);
            }
            else {
                vscode.window.showErrorMessage("You cannot do a refactoring on an unsaved file. Please save the file to the disk first.");
            }
        }
        else {
            vscode.window.showErrorMessage("There is nothing selected to refactor.");
        }
    }
}
exports.RenameLocalVariable = RenameLocalVariable;
//# sourceMappingURL=RenameLocalVariable.js.map