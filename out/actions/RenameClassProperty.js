"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RenameClassProperty = void 0;
const vscode = require("vscode");
const Editor_1 = require("../commons/Editor");
const FileSystem_1 = require("../commons/FileSystem");
const SystemShell_1 = require("../commons/SystemShell");
class RenameClassProperty {
    constructor() {
        this.editor = new Editor_1.Editor();
        this.fileSystem = new FileSystem_1.FileSystem();
        this.systemShell = new SystemShell_1.SystemShell();
    }
    refactor(context) {
        let thisClass = this;
        vscode.window.showInputBox({ prompt: "Enter the new name of the class property." }).then(function (classProperty) {
            if (classProperty !== null && classProperty.length > 0) {
                thisClass.renameClassProperty(classProperty, context);
            }
            else {
                vscode.window.showErrorMessage("The new class property name has to be longer than 0 characters.");
            }
        });
    }
    renameClassProperty(classProperty, context) {
        if (vscode.window.activeTextEditor !== null && this.editor.isLineRangeSelected()) {
            let line = this.editor.getStartLine();
            let oldClassProperty = this.editor.getSelectedText();
            let fileLocation = this.fileSystem.getFileLocation();
            if (fileLocation !== null) {
                let executionCommand = "rename-property " + fileLocation + " " + line + " " + oldClassProperty + " " + classProperty;
                let completeText = this.editor.getCompleteText();
                this.systemShell.executeCommand(executionCommand, context, completeText);
            }
            else {
                vscode.window.showErrorMessage("You cannot do a refactoring on an unsaved file. Please save the file to the disk first.");
            }
        }
        else {
            vscode.window.showErrorMessage("There is nothing selected to refactor.");
        }
    }
}
exports.RenameClassProperty = RenameClassProperty;
//# sourceMappingURL=RenameClassProperty.js.map