"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExtractMethod = void 0;
const vscode = require("vscode");
const Editor_1 = require("../commons/Editor");
const FileSystem_1 = require("../commons/FileSystem");
const SystemShell_1 = require("../commons/SystemShell");
class ExtractMethod {
    constructor() {
        this.editor = new Editor_1.Editor();
        this.fileSystem = new FileSystem_1.FileSystem();
        this.systemShell = new SystemShell_1.SystemShell();
    }
    refactor(context) {
        let thisClass = this;
        vscode.window.showInputBox({ prompt: "Enter the name of the new method." }).then(function (methodName) {
            if (methodName !== null && methodName.length > 0) {
                thisClass.extractMethod(methodName, context);
            }
            else {
                vscode.window.showErrorMessage("The method name has to be longer than 0 characters.");
            }
        });
    }
    extractMethod(methodName, context) {
        if (vscode.window.activeTextEditor !== null && this.editor.isLineRangeSelected()) {
            let startLine = this.editor.getStartLine();
            let endLine = this.editor.getEndLine();
            let fileLocation = this.fileSystem.getFileLocation();
            if (fileLocation !== null) {
                let executionCommand = "extract-method " + fileLocation + " " + startLine + "-" + endLine + " " + methodName;
                let completeText = this.editor.getCompleteText();
                this.systemShell.executeCommand(executionCommand, context, completeText);
            }
            else {
                vscode.window.showErrorMessage("You cannot do a refactoring on an unsaved file. Please save the file to the disk first.");
            }
        }
        else {
            vscode.window.showErrorMessage("There is nothing selected to refactor.");
        }
    }
}
exports.ExtractMethod = ExtractMethod;
//# sourceMappingURL=ExtractMethod.js.map