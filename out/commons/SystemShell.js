"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SystemShell = void 0;
const vscode = require("vscode");
const FileSystem_1 = require("./FileSystem");
class SystemShell {
    constructor() {
        this.fileSystem = new FileSystem_1.FileSystem();
    }
    executeCommand(command, context, completeText) {
        let phpExecutable = this.fileSystem.getPHPExecutable();
        if (phpExecutable !== null && phpExecutable !== "") {
            let libraryLocation = this.fileSystem.getLibraryLocation(context);
            let executionCommand = phpExecutable + " " + libraryLocation + " " + command;
            const cp = require('child_process');
            cp.exec(executionCommand, (err, stdout, stderr) => {
                if (err) {
                    vscode.window.showErrorMessage(stderr);
                }
                let result = this.patchText(stdout, completeText);
                vscode.window.activeTextEditor?.edit(function (editor) {
                    let activeTextEditor = vscode.window.activeTextEditor;
                    let activeDocument = activeTextEditor?.document;
                    let firstLine = activeDocument?.lineAt(0);
                    let lastLine = activeDocument?.lineAt(activeDocument.lineCount - 1);
                    editor.replace(new vscode.Range(firstLine.range.start, lastLine.range.end), result);
                    let position = activeTextEditor.selection.start;
                    activeTextEditor.selection = new vscode.Selection(position, position);
                });
            });
        }
    }
    patchText(executionResult, completeText) {
        let arrayOfExecutionResult = executionResult.split(/\n/);
        let arrayOfCompleteText = completeText.split(/\n/);
        let executionResultLine = "";
        let textLine = null;
        let result = "";
        let diff = 0;
        let startNext = 1;
        let diffNew = 0;
        let loop = false;
        let resultLineNumber = 2;
        while (arrayOfExecutionResult.length > 0 && !arrayOfExecutionResult[0].startsWith("---")) {
            arrayOfExecutionResult.shift();
        }
        for (let curLineNumber = 1; curLineNumber <= arrayOfCompleteText.length; curLineNumber++) {
            textLine = arrayOfCompleteText[curLineNumber - 1];
            if ((curLineNumber - diff) === startNext) {
                loop = true;
                while (loop) {
                    if (resultLineNumber >= arrayOfExecutionResult.length) {
                        result += textLine + "\n";
                        loop = false;
                    }
                    else {
                        executionResultLine = arrayOfExecutionResult[resultLineNumber];
                        if (executionResultLine.startsWith("@@")) {
                            diff = diffNew;
                            let ranges = executionResultLine.substring(3, executionResultLine.length - 3);
                            let rangesArray = ranges.split(" ");
                            let oldRange = rangesArray[0].substring(1).split(",");
                            let newRange = rangesArray[1].substring(1).split(",");
                            startNext = parseInt(oldRange[0]);
                            diffNew = parseInt(oldRange[0]) - parseInt(newRange[0]);
                            result += textLine + "\n";
                            loop = false;
                        }
                        else if (executionResultLine.startsWith("-")) {
                            startNext++;
                            loop = false;
                        }
                        else if (executionResultLine.startsWith("+")) {
                            result += executionResultLine.substring(1) + "\n";
                        }
                        else {
                            result += textLine + "\n";
                            startNext++;
                            loop = false;
                        }
                    }
                    resultLineNumber++;
                }
            }
            else {
                result += textLine;
                if (curLineNumber < arrayOfCompleteText.length) {
                    result += "\n";
                }
            }
        }
        return result;
    }
}
exports.SystemShell = SystemShell;
//# sourceMappingURL=SystemShell.js.map