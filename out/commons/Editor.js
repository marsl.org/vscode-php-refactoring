"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Editor = void 0;
const vscode = require("vscode");
class Editor {
    getCompleteText() {
        let completeText = vscode.window.activeTextEditor.document.getText();
        return completeText;
    }
    getStartLine() {
        let startLine = vscode.window.activeTextEditor.selection.start.line + 1;
        return startLine;
    }
    getEndLine() {
        let endLine = vscode.window.activeTextEditor.selection.end.line + 1;
        return endLine;
    }
    getSelectedText() {
        let selection = vscode.window.activeTextEditor?.selection;
        return vscode.window.activeTextEditor.document.getText(selection);
    }
    isLineRangeSelected() {
        let startLine = this.getStartLine();
        let endLine = this.getEndLine();
        let result = false;
        if (startLine !== endLine) {
            result = true;
        }
        else {
            let selection = vscode.window.activeTextEditor?.selection;
            let text = vscode.window.activeTextEditor?.document.getText(selection);
            result = text.length > 0;
        }
        return result;
    }
}
exports.Editor = Editor;
//# sourceMappingURL=Editor.js.map