"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileSystem = void 0;
const vscode = require("vscode");
class FileSystem {
    getFileLocation() {
        let fileLocation = vscode.window.activeTextEditor?.document.uri.fsPath;
        if (fileLocation.startsWith("Untitled")) {
            return null;
        }
        return fileLocation;
    }
    getPHPExecutable() {
        const conf = vscode.workspace.getConfiguration('php');
        let executablePath = conf.get('executablePath') ||
            conf.get('validate.executablePath') ||
            (process.platform === 'win32' ? 'php.exe' : 'php');
        if (executablePath === null || executablePath === "") {
            vscode.window.showErrorMessage("Cannot find the PHP executable path. Please set it as environment variable or in the Visual Studio Code settings.");
        }
        return executablePath;
    }
    getLibraryLocation(context) {
        return context.asAbsolutePath("refactor.phar");
    }
}
exports.FileSystem = FileSystem;
//# sourceMappingURL=FileSystem.js.map