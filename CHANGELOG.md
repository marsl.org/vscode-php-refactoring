# Change Log

### 1.3.0 on 07.02.2022

- Added support for PHP 7 and PHP 8.

### 1.2.0 on 08.11.2020

- Support for Rename Class Property refactoring.

### 1.1.0 on 07.11.2020

- Support for Rename Local Variable refactoring.

### 1.0.6 on 14.04.2020

- Fixed a bug where PHP warnings and notices would be take into account for the refactoring patch.

### 1.0.0 on 07.04.2020

- Initial release
- Support for Extract Method refactoring
